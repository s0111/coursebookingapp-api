//[SECTION] Dependencies and Modules
  const auth = require("../auth")
  const User = require('../models/User');
  const Course = require('../models/Course');
  //bcrypt is a package which will help us add a layer of security for our user's passwords.
  const bcrypt = require("bcrypt");
  const dotenv = require("dotenv"); 
  
//[SECTION] Environment Setup
  dotenv.config();
  const salt = 10; 

//[SECTION] Functionalities [CREATE]
  module.exports.registerUser = (data) => {
  	let fName = data.firstName;
  	let lName = data.lastName;
  	let uemail = data.email;
  	let passW= data.password;
  	let gendr = data.gender;
  	let mobil = data.mobileNo;
    
  	let newUser = new User({
  		firstName: fName,
  		lastName: lName,
  		email: uemail,
  		password: bcrypt.hashSync(passW, salt),
  		gender: gendr,
  		mobileNo: mobil 
  	}); 
  	

    return User.findOne({email: uemail}).then((user) => {

      if(user){
          return (`User already exists`)
      }else {
          return newUser.save().then((savedUser, error) => {
         
              if (error) {
                   
                  return 'Failed to Save New User';
              } else {
                  
                  return savedUser; 
              }
          });
      }
  });
}

  /*
    bcrypt adds a layer of security to our user's password.

    What bcrypt does is hash our password into a randomized character version of the original string.

    It is able to hide your password within that randomized string.
    
    syntax:
    bcrypt.hashSync(<stringToBeHashed>,<saltRounds>)

    Salt-Rounds are the number of times the characters in the hash are randomized

  */



// [SECTION] retrieve
module.exports.getAllUser = () => {
  return User.find({}).then(result => {
    return result;
  });
}



// !LOGIN 

module.exports.loginUser = (req, res) => {

  // Do we need a user input in this controller?
    // YES
  // Where can we get this user input?
    // req.body (request body)

    console.log(req.body)

    /*
      Steps:
      1. Find the user by the email
      2. If we found a user, we will check the password
      3. If we don't find a user, we will send a message to the client
      4. If upon checking the found user's password is the same as the input's password, we will generate a "key" to access our app. If not, we will turn them away by sending a message to the client

    */
    User.findOne({email: req.body.email})
    .then(foundUser => {

      if(foundUser === null){
        return res.send("User Not Found");

      } else {

        const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

        console.log(isPasswordCorrect)

        /*
          syntax:
          bcrypt.compareSync(<string>, <hashedString>)

          if the string and the hashedString matches, compareSync method from bcrypt, will return true

          if not, it will return false
        */
        
        if(isPasswordCorrect){

          return res.send({accessToken: auth.createAccessToken(foundUser)})

        } else {

          return res.send("Incorrect Password")
        }

      }

    })
    .catch(err => res.send(err))

};


//[SECTION] Functionalities [RETRIEVE]
// GET USER DETAILS

module.exports.getUserDetails = (req, res) => {

    console.log(req.user)

    /*
      Step/s:
      1. Find our logged in user's document from our db and send it to the client by its id

    */

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(err => res.send(err))
};
	
//!Enroll a registered User
/*
Enrollment Steps:

1. Look for the user by its id.
  -push the details of the course we're trying to enroll in. We'll push to a new enrollment subdocument in our user (enrollments).
2. Look for the course by its id.
  -push the details of the enrolee/user who's trying to enroll. We'll push to a new enrollees subdocument in our user (enrollees).

3. When both saving of documents are successful, we send a message to the client.

*/


//async/await function
//fetch .then
//promise


module.exports.enroll = async (req, res) => {
  //get the details of the user who's trying to enroll in req.user
  /*
      req.user 
        {
          id,
          email,
          isAdmin
        }

  */

  console.log(req.user.id) //the user's id from the decoded token after verify()
  console.log(req.params.id) //the course from our request body

  //process stops here and sends response IF user is an admin
  if(req.user.isAdmin){
    return res.send("Action Forbidden")
  }

  /*
  async - async keyword allows us to make our function asynchronous. which means, that instead of JS regular behaviour of running eac code line by line, It will allow us to wait for the result of function.

  await -  we use this to wait for the result of a function to finsih before proceeding to the next statement.



  */

  let isUserUpdated = await User.findById(req.user.id).then(user => {



    //Add the courseId in an object and push that object into the user's enrollment array:
    let newEnrollment = {
        courseId: req.params.id
    }
 
    //access the enrollments array from our user and push the new enrollment object into the array
    user.enrollments.push(newEnrollment);

    //save the changes made to our user document and return the value of saving our document
    //if we properly saved our document, isUserUpdated will contain the boolean true
    //if we catch an error, isUserUpdated will contain the error message
    return user.save().then(user => true).catch(err => err.message)


  })

  //if isUserUpdated contains the boolean true, then the saving of our user document successful
  //if isUserUpdated does not contain the boolean true, we will stop our process and return a res.send() to our client with our message
  if(isUserUpdated !== true) {
      return res.send({message: isUserUpdated})
  }


  //Find the course we will push for our enrollee array 
  let isCourseUpdated = await Course.findById(req.params.id).then(course => {


    let enrollee = {
        userId: req.user.id
    }

    course.enrollees.push(enrollee);

    return course.save().then(course => true).catch(err => err.message)


  })

  //Stop the process if their was an error saving our course document
  if(isCourseUpdated !== true) {
      return res.send({ message: isCourseUpdated})
  }


  if(isUserUpdated && isCourseUpdated) {
      return res.send({ message: "Enrolled Successfully."})
  }


}



module.exports.getEnrollments = (req, res) => {
    User.findById(req.user.id).then(result => res.send(result.enrollments))
    .catch(err => res.send(err))
}


//[SECTION] Functionalities [UPDATE]
//[SECTION] Functionalities [DELETE]
